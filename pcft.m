function pcft(pathname, movie_name, nColparts, nRowparts)

addpath 'cft';
npart = nColparts * nRowparts + 1;

fp_nframes = fopen([[pathname, movie_name, '/'], movie_name, '_frames.txt'],'r');
tline = fgetl(fp_nframes);
tdata = sscanf(tline, '%d,%d');
startFrm = tdata(1,1);
endFrm = tdata(2,1);
nFrames = endFrm - startFrm + 1;

% output result, Rui Yao, 2015-10-22
% fp_pcft_gt = fopen([[pathname, movie_name, '/'], movie_name, '_pcft_gt.txt'],'w');

time = 0;
target_sz = cell(1, npart);
window_sz = cell(1, npart);
cos_window = cell(1, npart);
yf = cell(1, npart);
padding = 1.5;  %extra area surrounding the target
lambda = 1e-4;  %regularization
output_sigma_factor = 0.1;  %spatial bandwidth (proportional to target)
cell_size = 4;
features.gray = false;
features.hog = true;
features.hog_orientations = 9;
interp_factor = 0.02; % linear interpolation factor for adaptation
kernel.type = 'gaussian';
kernel.sigma = 0.5; % gaussian kernel bandwidth
kernel.poly_a = 1; % polynomial kernel additive term
kernel.poly_b = 9; % polynomial kernel exponent

part_parent = cell(1, npart);
bbpre = cell(1, npart);
bbshow = cell(1, npart);

for i=1:nFrames
    tic()
    img = imread(sprintf('%s%s%s%05d.jpg', pathname, movie_name, '/img/img', startFrm+i-1)); 
%     img = imread(sprintf('%s%s%s%04d.jpg', pathname, movie_name, '/imgs/', startFrm+i-1)); 
    if ~ismatrix(img)
        img = rgb2gray(img);
    end
    
    % Set up some parameters for the first time
    if i == 1
        % Read ground truth location for first frame
        fp_gt = fopen([[pathname, movie_name, '/'], movie_name, '_gt.txt'],'r');
        tline = fgetl(fp_gt);
        if ~ischar(tline) break; end
        tdata = sscanf(tline, '%f,%f,%f,%f');
        locat = cell(1, npart);
        locat{1} = [tdata(1,1), tdata(2,1), tdata(3,1), tdata(4,1)];

        partCnt = 1;
        partWidth = locat{1}(1,3)/nColparts;
        partHeight = locat{1}(1,4)/nRowparts;
        for c = 0:nColparts-1
            for r = 0:nRowparts-1
                locat{partCnt+1} = [locat{1}(1,1)+c*partWidth, locat{1}(1,2)+r*partHeight, partWidth, partHeight];
                partCnt = partCnt + 1;
            end
        end
        
        bb = cell(1,npart);
        bbcenter = cell(1,npart);
        bbout = cell(1,npart);
        location = cell(1,npart);
        partloc = [];
        objrect = cell(1,npart);
        
        % model, Rui Yao, 2015-12-7
        model_alphaf = cell(1, npart);
        model_xf = cell(1, npart);
        
        imshow(img);
        
        for j=1:npart
            bb{j}=round([locat{j}(1,1) locat{j}(1,2) locat{j}(1,1)+locat{j}(1,3) locat{j}(1,2)+locat{j}(1,4)]);
            bbcenter{j}=floor(0.5*([bb{j}(2)+bb{j}(4) bb{j}(1)+bb{j}(3)]));
            bbout{j} = bb{j}';
            scalepre{j} = 1;
            valid(j)=1;
            partloc=[partloc;bbcenter{j}];
            
            target_sz{j} = [locat{j}(1,4) locat{j}(1,3)];
            % window size, taking padding into account
            window_sz{j} = floor(target_sz{j} * (1 + padding));
            
            %create regression labels, gaussian shaped, with a bandwidth
            %proportional to target size
            output_sigma = sqrt(prod(target_sz{j})) * output_sigma_factor / cell_size;
            yf{j} = fft2(gaussian_shaped_labels(output_sigma, floor(window_sz{j} / cell_size)));
            %store pre-computed cosine window
            cos_window{j} = hann(size(yf{j},1)) * hann(size(yf{j},2))';	
            
            % Initialize model, obtain a subwindow for training at newly estimated target position
            patch = get_subwindow(img, bbcenter{j}, window_sz{j});
            model_xf{j} = fft2(get_features(patch, features, cell_size, cos_window{j}));
            % Kernel Ridge Regression, calculate alphas (in Fourier domain)
            kf = gaussian_correlation(model_xf{j}, model_xf{j}, kernel.sigma);
            model_alphaf{j} = yf{j} ./ (kf + lambda);   %equation for fast training
        end
        
        % Build minimum spanning tree
        [ tree, root, nodepath ] = CreateTree( img, partloc );
        % find the parent of a node, see section 4.1.1 of [Pictorial Structures for Object Recognition, Felzenszwalb 2005] for details.
        for k=1:npart
            for j=1:length(nodepath)
                ind=find(k==nodepath{j});
                if ind==length(nodepath{j})
                    parent=nodepath{j}(ind);
                elseif isempty(ind)==0
                    parent=nodepath{j}(ind+1);
                end
            end
            % distance between part and parent in col and row direction, i.e. edge vector
            part_parent{k}=[(bbout{parent}(2)+bbout{parent}(4))/2-(bbout{k}(2)+bbout{k}(4))/2 (bbout{parent}(1)+bbout{parent}(3))/2-(bbout{k}(1)+bbout{k}(3))/2];
        end
    else
        % Run detector
        for k=1:npart
            bbpre{k}=location{k}(i-1,:);
        end
        
        [bbout, bbcenter, scale, model_alphaf, model_xf, valid, inconf_map1, part_parent] = ...
            cft(part_parent, root, nodepath, valid, npart, bbpre, scalepre, img, ...
            model_alphaf, model_xf, bb, window_sz, cos_window,...
            yf, bbcenter, features, kernel, cell_size, interp_factor, lambda);
        scalepre = scale;
        
        % Confidence map
        imtem=[];
        for j=1:npart
            imtem = [imtem;inconf_map1{j}];
        end
    end
    
    % Plot bounding box
    if i==1
        imtemp=0.*imresize(img,[size(img,1) round(size(img,2)/npart)]);
    else
        imtemp=255.*imresize(imtem,[size(img,1) round(size(img,2)/npart)]);
    end
    imgout=[img imtemp];
    
    if i==1
        handle = imshow(imgout);
    else
        set(handle,'cdata',imgout); hold on;
    end
    axis off
    
    if i>1
        for j=1:npart
            delete(objrect{j})
        end
        
        delete(frmtext);
    end
    for j=1:npart
        bbshow{j}=[bbout{j}(1) bbout{j}(2) bbout{j}(3)-bbout{j}(1) bbout{j}(4)-bbout{j}(2)];
        
        color = {'yellow', 'magenta', 'green', 'blue', 'red'};
        if valid(j)==1 % high confidence
            objrect{j} = rectangle('Position', bbshow{j},'EdgeColor',color{j},'LineWidth',2,'LineStyle','-');
        else  %low confidence
            objrect{j} = rectangle('Position', bbshow{j},'EdgeColor',color{j},'LineWidth',2,'LineStyle','-.');
        end
        location{j}(i,:) = bbout{j};
        
        img_cols = size(img,2); img_rows = size(img,1);
        position_c = img_cols/64; position_r = img_rows/16;
    end
    
%     fprintf(fp_pcft_gt, '%d,%d,%d,%d\n', bbshow{1}(1,1), bbshow{1}(1,2), bbshow{1}(1,3), bbshow{1}(1,4));
    frmtext = text(position_c, img_rows - position_r, ['#',num2str(i)], 'Color', 'y', 'FontWeight','bold', ...
            'FontSize', img_cols/18); % bold
        
%     if ispc
        drawnow;
%     end
    hold off;
    time = time + toc();
    %toc;
end

disp(['fps = ', num2str(1/(time / nFrames))] );
% fclose(fp_pcft_gt);
