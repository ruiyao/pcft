function [bbout, bbcenout, scaleout, model_alphaf, model_xf, valid, inconf_map, part_parent] = ...
    cft(part_parent, root, nodepa, valid, npart, ~, scalepre, im, ...
    model_alphaf, model_xf, bb, window_sz, cos_window, yf, pos, features,...
    kernel, cell_size, interp_factor, lambda)

% allocate some memory
width    = cell(1, npart);
height   = cell(1, npart);
bbcenout = cell(1, npart);
bbout    = cell(1, npart);
scaleout = cell(1, npart);
posvector= cell(1, npart);
Ix1      = cell(1, npart);
Iy1      = cell(1, npart);
part_cen = cell(1, npart);
map      = cell(1, npart);
loc_temp = cell(1, npart);
inconf_map = cell(1, npart);
confidence = cell(1, npart);
conf_part  = cell(1, npart);
part_partemp = zeros(npart, 2);
response_sz = cell(1, npart);

for i=1:npart
    % pixel size of object
    height{i}=bb{i}(4)-bb{i}(2);  % bb[y1 x1 y2 x2]
    width{i}=bb{i}(3)-bb{i}(1);
end

traff1=0.001; %0.001
traff2=0;
strcuture_threshold = 2000;
structure_para = 1.05;

%% calculate confidence for each part. Rui Yao
for i=1:npart
    % ---------------------- test, Rui Yao, 2015-12-7 ------------------------
    patch = get_subwindow(im, pos{i}, window_sz{i});
    zf = fft2(get_features(patch, features, cell_size, cos_window{i}));

    %calculate response of the classifier at all shifts
    kzf = gaussian_correlation(zf, model_xf{i}, kernel.sigma);
    response = real(ifft2(model_alphaf{i} .* kzf));  %equation for fast detection

    % -------------------- Incorporate edge deformation. -----------------------
    % shift the response, Rui Yao, 2015-12-8
    zero_num = cell_size - 1;
    response_sz{i} = size(response);
    tmp_conf = fftshift(response);
    confidence{i} = zeros(size(tmp_conf,1)*zero_num+size(tmp_conf,1), size(tmp_conf,2)*zero_num+size(tmp_conf,2));
    confidence{i}(1:zero_num+1:end,1:zero_num+1:end) = tmp_conf(:,:);
    inconf_map{i} = zeros(size(im,1),size(im,2));
    confidence_sz = size(confidence{i});
    tmp_pos = pos{i} - cell_size * floor(size(response)/2); 
	if tmp_pos(2) < 1, tmp_pos(2) = 1; end
    if tmp_pos(1) < 1, tmp_pos(1) = 1; end
    
    sub_left = 1;
    if pos{i}(2) - confidence_sz(2)/2 < 0, 
        sub_left = abs(pos{i}(2) - confidence_sz(2)/2); 
    end
    sub_width = confidence_sz(2);
    if pos{i}(2) + confidence_sz(2)/2 > size(im, 2), 
        sub_width = confidence_sz(2) - (pos{i}(2) + confidence_sz(2)/2 - size(im, 2)); 
    end
    sub_top = 1;
    if pos{i}(1) - confidence_sz(1)/2 < 0, 
        sub_top = abs(pos{i}(1) - confidence_sz(1)/2); 
    end
    sub_height = confidence_sz(1);
    if pos{i}(1) + confidence_sz(1)/2 > size(im, 1), 
        sub_height = confidence_sz(1) - (pos{i}(1) + confidence_sz(1)/2 - size(im, 1)); 
    end

    inconf_map{i}(tmp_pos(1):tmp_pos(1)+sub_height-sub_top, tmp_pos(2):tmp_pos(2)+sub_width-sub_left) = ...
        confidence{i}(sub_top:sub_height, sub_left:sub_width);
    
    inconf_map{i}(inconf_map{i}<0)=0;
    inconf_map{i} = inconf_map{i}(1:size(im,1),1:size(im,2));

    % This transformation spreads high scores to nearby locations, taking into account the deformation costs.
    [inconf_map{i}, Ix1{i}, Iy1{i}] = dt(inconf_map{i},traff1,traff2,traff1,traff2);
    
    part_cen{i}=[0.1 0.1];
    part_partemp(i,:)=part_parent{i};
end

%% Get Score Map
% 1. Get path from node to root, part_cen{i} is the offset between part i to root by following the node path. Rui Yao
part_parent{root}=[0 0];
part_partemp(root,:)=[0 0];
part_cen{root}=[0 0];
for i=1:length(nodepa)
    n=length(nodepa{i});
    temp = part_partemp(nodepa{i},:);
    for j=n-1:-1:1
        if part_cen{nodepa{i}(j)}(1)~=0 && part_cen{nodepa{i}(j)}(2)~=0
            part_cen{nodepa{i}(j)}=sum(temp(j:n-1,:),1);
        end
    end
end

% 2. Score map
% According to the relative position of each part with respect to the root, Rui Yao
suminconf_map=zeros(size(inconf_map{i},1),size(inconf_map{i},2));
for i=1:npart
    part_cen{i}=round(part_cen{i});
    map{i}=zeros(size(suminconf_map,1),size(suminconf_map,2));
    if part_cen{i}(1)<=0 && part_cen{i}(2)<=0
        map{i}(1:size(map{i},1)-abs(part_cen{i}(1)),1:size(map{i},2)-abs(part_cen{i}(2)))= inconf_map{i}(abs(part_cen{i}(1))+1:size(map{i},1),abs(part_cen{i}(2))+1:size(map{i},2));
    elseif part_cen{i}(1)<=0 && part_cen{i}(2)>0
        map{i}(1:size(map{i},1)-abs(part_cen{i}(1)),part_cen{i}(2)+1:size(map{i},2))= inconf_map{i}(abs(part_cen{i}(1))+1:size(map{i},1),1:size(map{i},2)-part_cen{i}(2));
    elseif part_cen{i}(1)>0 && part_cen{i}(2)<=0
        map{i}(part_cen{i}(1)+1:size(map{i},1),1:size(map{i},2)-abs(part_cen{i}(2)))= inconf_map{i}(1:size(map{i},1)-part_cen{i}(1),abs(part_cen{i}(2))+1:size(map{i},2));
    else
        map{i}(part_cen{i}(1)+1:size(map{i},1),part_cen{i}(2)+1:size(map{i},2))= inconf_map{i}(1:size(map{i},1)-part_cen{i}(1),1:size(map{i},2)-part_cen{i}(2));
    end
    suminconf_map=suminconf_map+map{i};
    % no shift accumulate
    %     suminconf_map=suminconf_map+inconf_map{i};
end
inconfi1 = max(suminconf_map(:));
[x1,y1]=find(suminconf_map==inconfi1);

%% find the tracking result of parts
for i=1:npart
    scaleout{i}=scalepre{i};
    % get the possible position of part according to the optimal location of root.
    loc_temp{i}=round([mean(x1) mean(y1)]-round(part_cen{i}*scalepre{i}));
    if loc_temp{i}(1)<1 || loc_temp{i}(1)>size(Iy1{i},1) || loc_temp{i}(2)<1 || loc_temp{i}(2)>size(Iy1{i},2)
        loc_temp{i}(1)=round(min(size(Iy1{i},1),max(1,loc_temp{i}(1))));
        loc_temp{i}(2)=round(min(size(Iy1{i},2),max(1,loc_temp{i}(2))));
    end
    loc=double([Iy1{i}(loc_temp{i}(1),loc_temp{i}(2)),Ix1{i}(loc_temp{i}(1),loc_temp{i}(2))]);
    
    [optcenter(1), optcenter(2)] = find(confidence{i}==max(confidence{i}(:)),1);
    % compare the optimal of sum confidence with the optimal of part confidence
    tmp_optcenter = pos{i} - cell_size * floor(response_sz{i}/2) + [optcenter(1) - 1, optcenter(2) - 1];
    if sum((loc-tmp_optcenter).^2)<strcuture_threshold
        conf_part{i} = confidence{i}(optcenter(1), optcenter(2));
        bbcenout{i} = tmp_optcenter;
    else
        % find the suboptimal, i.e. find the minimum distance between the optimal of sum location and the part
        conf_part{i}=inconf_map{i}(loc(1), loc(2));
        bbcenout{i} = loc;
%         disp('>200');
    end

	bbout{i} = [bbcenout{i}(2) - floor(width{i}/2), bbcenout{i}(1) - floor(height{i}/2), ...
        bbcenout{i}(2) + floor(width{i}/2), bbcenout{i}(1) + floor(height{i}/2)];

	% ------------------ update part tracker, Rui Yao, 2015-12-7 -------------------
    % obtain a subwindow for training at newly estimated target position
    patch = get_subwindow(im, bbcenout{i}, window_sz{i});
    xf = fft2(get_features(patch, features, cell_size, cos_window{i}));

    % Kernel Ridge Regression, calculate alphas (in Fourier domain)
    kf = gaussian_correlation(xf, xf, kernel.sigma);
    alphaf = yf{i} ./ (kf + lambda);   %equation for fast training

    %subsequent frames, interpolate model
    model_alphaf{i} = (1 - interp_factor) * model_alphaf{i} + interp_factor * alphaf;
    model_xf{i} = (1 - interp_factor) * model_xf{i} + interp_factor * xf;
    
    if conf_part{i}>0.4 
        valid(i)=1;
    else
        % Here, it didn't change the appearance of part i.
        valid(i)=0;
    end
end

%% update edge vector
for i=1:npart
    if valid(i) 
        for j=1:length(nodepa)
            ind=find(i==nodepa{j});
            if ind==length(nodepa{j})
                parent=nodepa{j}(ind);
            elseif isempty(ind)==0
                parent=nodepa{j}(ind+1);
            end
        end
        
        % update edge vector
        posvector{i}=bbcenout{parent}-bbcenout{i};
        part_parent{i}=(part_parent{i}/scaleout{i}+0.05*posvector{i})/structure_para;
    end
end

end

