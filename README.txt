Part-based Correlation Filter with Spatial Constraints (v1.0)

This code accompanies the paper:
Rui Yao, Shixiong Xia, Fumin Shen, Yong Zhou, and Qiang Niu. 
Exploiting Spatial Structure from Parts for Adaptive Kernelized Correlation Filter Tracker.
IEEE Signal Processing Letters 23, no. 5 (2016): 658-662.

Contact: Rui Yao (ruiyao@cumt.edu.cn)

------------
License
------------

  THIS SOFTWARE IS PROVIDED BY LU ZHANG AND AURENS VAN DER MAATEN ''AS IS'' AND ANY EXPRESS
  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO 
  EVENT SHALL LU ZHANG AND LAURENS VAN DER MAATEN BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
  OF SUCH DAMAGE.


------------
Citations
------------

In case you use PCFT code in your work, please cite the following paper:

@article{yao2016exploiting,
  title={Exploiting Spatial Structure from Parts for Adaptive Kernelized Correlation Filter Tracker},
  author={Yao, Rui and Xia, Shixiong and Shen, Fumin and Zhou, Yong and Niu, Qiang},
  journal={IEEE Signal Processing Letters},
  volume={23},
  number={5},
  pages={658--662},
  year={2016},
  publisher={IEEE}
}

------------
Requirements
------------

This code has been developed and tested with Ubuntu 14.04, Matlab R2015a (64-bit).

This is the first version of our code online. The tracking scale is fixed to one scale. One test video (car) and its annotations (every 5 frames) are provided with the code. The other movies we used in our experiments are available in separate files from the tracking benchmark website. We are still working on making a more efficient version of our code publicly available.

-----
Usage
-----

>> demo

The demo code can straightforwardly be adapted to run on other movies. Make sure that each annotation file contains a 1x4 vector, named location, that contains the bounding box location of the object of interest. The location-vector takes the form [y x w h], indicating the top-left corner and the width and height of the bounding box.
